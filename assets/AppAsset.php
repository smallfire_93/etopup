<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/main.css',
        'etopup/metisMenu/dist/metisMenu.min.css',
        'etopup/css/timeline.css',
        'etopup/css/sb-admin-2.css',
        'etopup/morrisjs/morris.css',
        'etopup/font-awesome/css/font-awesome.min.css',
        'etopup/css/style.css',
    ];
    public $js = [
		'etopup/bootstrap/dist/js/bootstrap.min.js',
        'etopup/metisMenu/dist/metisMenu.min.js',
        'etopup/js/sb-admin-2.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
