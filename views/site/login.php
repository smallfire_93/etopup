<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Đăng nhập';
?>

<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
    <h1>LOGIN</h1>
<?= $form->field($model, 'username')->textInput(['placeholder' => 'Username'])->label(false) ?>
<?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password'])->label(false) ?>
    <div class="form-group">
        <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    </div>
    <div class="clearfix"></div>
<?php ActiveForm::end(); ?>