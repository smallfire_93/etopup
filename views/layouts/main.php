<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;margin-bottom: -20px;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= Yii::$app->homeUrl ?>">Etopup Admin</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="<?= Yii::$app->urlManager->createUrl('site/logout') ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <!--                    <li>-->
                    <!--                        <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>-->
                    <!--                    </li>-->
                    <li>
                        <a href="<?= Yii::$app->urlManager->createUrl('dealer/dealer/index') ?>"><i class="fa fa-users fa-fw"></i> Dealer</a>
                    </li>
                    <li>
                        <a href="<?= Yii::$app->urlManager->createUrl('') ?>"><i class="fa fa-history fa-fw"></i> Topup History</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-money fa-fw"></i> Recharge<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?= Yii::$app->urlManager->createUrl('content/bill-config/index') ?>">Recharge History</a>
                            </li>
                            <li>
                                <a href="<?= Yii::$app->urlManager->createUrl('content/animal/index') ?>">Recharge</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="<?= Yii::$app->urlManager->createUrl('') ?>"><i class="fa fa-bar-chart fa-fw"></i> Invoice</a>
                    </li>
                    <li>
                        <a href="<?= Yii::$app->urlManager->createUrl('') ?>"><i class="fa fa-list fa-fw"></i> List Draw</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-cogs fa-fw"></i> Promotion<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?= Yii::$app->urlManager->createUrl('content/bill-config/index') ?>">Config promotion</a>
                            </li>
                            <li>
                                <a href="<?= Yii::$app->urlManager->createUrl('content/animal/index') ?>">List promotion</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Admin<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?= Yii::$app->urlManager->createUrl('/admin/assignment/index') ?>">Assignment</a>
                            </li>
                            <li>
                                <a href="<?= Yii::$app->urlManager->createUrl('/admin/role/index') ?>">Role</a>
                            </li>
                            <li>
                                <a href="<?= Yii::$app->urlManager->createUrl('/admin/route/index') ?>">Route</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper" class="row">
        <?= $content ?>
    </div>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
