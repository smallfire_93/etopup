<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Dealer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dealers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dealer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'password',
            'quota_type',
            'quota_default',
            'balance',
            'total_balance',
            'status',
            'msisdn',
            'email:email',
            'first_name',
            'last_name',
            'created_at',
            'updated_at',
            'created_by',
            'address',
            'province_id',
            'district_id',
            'village_id',
            'parent_id',
            'updated_by',
            'seller_id',
            'seller_type',
            'photo',
            'discount',
            'request_id',
            'login_time',
            'count_login_fail',
            'block_by',
            'block_time',
        ],
    ]) ?>

</div>
