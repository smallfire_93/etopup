<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DealerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dealers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dealer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dealer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'password',
            'quota_type',
            'quota_default',
            // 'balance',
            // 'total_balance',
            // 'status',
            // 'msisdn',
            // 'email:email',
            // 'first_name',
            // 'last_name',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'address',
            // 'province_id',
            // 'district_id',
            // 'village_id',
            // 'parent_id',
            // 'updated_by',
            // 'seller_id',
            // 'seller_type',
            // 'photo',
            // 'discount',
            // 'request_id',
            // 'login_time',
            // 'count_login_fail',
            // 'block_by',
            // 'block_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
