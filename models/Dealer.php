<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dealer".
 *
 * @property string $id
 * @property string $username
 * @property string $password
 * @property integer $quota_type
 * @property integer $quota_default
 * @property integer $balance
 * @property string $total_balance
 * @property integer $status
 * @property string $msisdn
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property string $address
 * @property integer $province_id
 * @property integer $district_id
 * @property integer $village_id
 * @property integer $parent_id
 * @property integer $updated_by
 * @property string $seller_id
 * @property integer $seller_type
 * @property string $photo
 * @property integer $discount
 * @property string $request_id
 * @property string $login_time
 * @property integer $count_login_fail
 * @property string $block_by
 * @property string $block_time
 */
class Dealer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dealer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'status', 'created_at', 'created_by'], 'required'],
            [['quota_type', 'quota_default', 'balance', 'total_balance', 'status', 'created_by', 'province_id', 'district_id', 'village_id', 'parent_id', 'updated_by', 'seller_type', 'discount', 'count_login_fail'], 'integer'],
            [['created_at', 'updated_at', 'login_time', 'block_time'], 'safe'],
            [['username', 'password', 'email', 'first_name', 'last_name', 'block_by'], 'string', 'max' => 50],
            [['msisdn'], 'string', 'max' => 16],
            [['address'], 'string', 'max' => 500],
            [['seller_id'], 'string', 'max' => 20],
            [['photo'], 'string', 'max' => 255],
            [['request_id'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'quota_type' => 'Quota Type',
            'quota_default' => 'Quota Default',
            'balance' => 'Balance',
            'total_balance' => 'Total Balance',
            'status' => 'Status',
            'msisdn' => 'Msisdn',
            'email' => 'Email',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'address' => 'Address',
            'province_id' => 'Province ID',
            'district_id' => 'District ID',
            'village_id' => 'Village ID',
            'parent_id' => 'Parent ID',
            'updated_by' => 'Updated By',
            'seller_id' => 'Seller ID',
            'seller_type' => 'Seller Type',
            'photo' => 'Photo',
            'discount' => 'Discount',
            'request_id' => 'Request ID',
            'login_time' => 'Login Time',
            'count_login_fail' => 'Count Login Fail',
            'block_by' => 'Block By',
            'block_time' => 'Block Time',
        ];
    }
}
