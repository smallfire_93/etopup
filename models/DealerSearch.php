<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dealer;

/**
 * DealerSearch represents the model behind the search form about `app\models\Dealer`.
 */
class DealerSearch extends Dealer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'quota_type', 'quota_default', 'balance', 'total_balance', 'status', 'created_by', 'province_id', 'district_id', 'village_id', 'parent_id', 'updated_by', 'seller_type', 'discount', 'count_login_fail'], 'integer'],
            [['username', 'password', 'msisdn', 'email', 'first_name', 'last_name', 'created_at', 'updated_at', 'address', 'seller_id', 'photo', 'request_id', 'login_time', 'block_by', 'block_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dealer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'quota_type' => $this->quota_type,
            'quota_default' => $this->quota_default,
            'balance' => $this->balance,
            'total_balance' => $this->total_balance,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'province_id' => $this->province_id,
            'district_id' => $this->district_id,
            'village_id' => $this->village_id,
            'parent_id' => $this->parent_id,
            'updated_by' => $this->updated_by,
            'seller_type' => $this->seller_type,
            'discount' => $this->discount,
            'login_time' => $this->login_time,
            'count_login_fail' => $this->count_login_fail,
            'block_time' => $this->block_time,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'msisdn', $this->msisdn])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'seller_id', $this->seller_id])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'request_id', $this->request_id])
            ->andFilterWhere(['like', 'block_by', $this->block_by]);

        return $dataProvider;
    }
}
